package org.mardld.webchat.service;

public interface ExternalPropertiesService {

    /**
     * Gets find-friend service api base url.
     * Example: https://localhost:8080/friend-find-service/api
     * @return
     */
    String getFindFriendApiBaseUrl();
}
