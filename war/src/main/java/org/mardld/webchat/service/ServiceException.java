package org.mardld.webchat.service;

public class ServiceException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8265883167422657506L;
    
    public ServiceException(String message) {
        super(message);
    }
    
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
