package org.mardld.webchat.service.impl;

import org.mardld.webchat.service.ExternalPropertiesService;
import org.mardld.webchat.service.FriendFindService;
import org.mardld.webchat.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class RemoteFriendFindService implements FriendFindService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteFriendFindService.class);
    
    @Autowired
    private ExternalPropertiesService externalPropertiesService;
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Override
    public Friend getFriend() throws ServiceException {
        LOGGER.info("getFriend:: ");
        String url = externalPropertiesService.getFindFriendApiBaseUrl() + "/v1/test";
        Friend friend = restTemplate.getForObject(url, Friend.class);
        LOGGER.info("getFriend:: returning {}", friend);
        return getFriend("yolo");
    }
    
    public Friend getFriend(String friendId) throws ServiceException {
        LOGGER.info("getFriend:: friendId={}", friendId);
        
        String url = externalPropertiesService.getFindFriendApiBaseUrl() + "/v1/test";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Custom-Header", friendId);
        
        HttpEntity<?> reqEntity = new HttpEntity<>(headers);
        ResponseEntity<Friend> rspEntity = restTemplate.exchange(url, HttpMethod.GET, reqEntity, Friend.class);

        LOGGER.debug("getFriend:: got response {}", rspEntity.getStatusCode());
        if (rspEntity.getStatusCode() != HttpStatus.OK) {
            return null;
        }
        
        Friend friend = rspEntity.getBody();
        LOGGER.info("getFriend:: returning {}", friend);
        return friend;
    }
}
