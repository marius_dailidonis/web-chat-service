package org.mardld.webchat.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import org.mardld.webchat.service.ExternalPropertiesService;
import org.mardld.webchat.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class FileSystemBasedExternalPropertiesService implements ExternalPropertiesService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemBasedExternalPropertiesService.class);
    
    Properties props;
    
    public FileSystemBasedExternalPropertiesService() throws ServiceException {
        String propertiesFile = System.getProperty("webchat.propertiesFile");
        if (propertiesFile == null) {
            throw new ServiceException("Properties load failed. Required JVM arg 'webchat.propertiesFile' is missing.");
        }
        
        try {
            props = new Properties();
            props.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            String msg = MessageFormat.format("properties load from [{0}] failed. Cause: {1}", propertiesFile,
                    e.getMessage());
            LOGGER.error("ctor:: " + msg, e);
            throw new ServiceException(msg, e);
        }
    }
    
    @Override
    public String getFindFriendApiBaseUrl() {
        return props.getProperty("find-service-api-base-url");
    }
    
}
