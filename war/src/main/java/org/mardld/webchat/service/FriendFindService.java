package org.mardld.webchat.service;

public interface FriendFindService {

    /**
     * 
     * @return
     * @throws ServiceException 
     */
    Friend getFriend() throws ServiceException;
    
    class Friend {
        
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("Friend [name=%s]", name);
        }
        
        
    }
}
