/**
 * 
 */
package org.mardld.webchat.controller;

import org.mardld.webchat.service.FriendFindService;
import org.mardld.webchat.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author marius
 *
 */
@RestController
@RequestMapping("/api/v1")
public class ApiController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private FriendFindService friendFindService;
    
    @RequestMapping(value = "/test", produces = {
            MediaType.APPLICATION_JSON_VALUE
    })
    public SayHelloResponse sayHello() {
        LOGGER.info("sayHello:: ");
        return new SayHelloResponse();
    }
    
    @RequestMapping(value="/friends", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public FriendFindService.Friend getFriend() throws ServiceException {
        LOGGER.info("getFriend:: ");

        FriendFindService.Friend friend = friendFindService.getFriend();
        LOGGER.info("getFriend:: returns {}", friend);
        return friend;
    }

}

class SayHelloResponse {
    
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
