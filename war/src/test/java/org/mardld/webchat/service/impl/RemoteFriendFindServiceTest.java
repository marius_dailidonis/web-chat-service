package org.mardld.webchat.service.impl;

import static org.mockito.Mockito.when;

import org.mardld.webchat.service.ExternalPropertiesService;
import org.mardld.webchat.service.FriendFindService;
import org.mardld.webchat.service.FriendFindService.Friend;
import org.mardld.webchat.service.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RemoteFriendFindServiceTest {

    @Mock
    private ExternalPropertiesService externalPropertiesService;
    
    @Mock
    private RestTemplate restTemplate;
    
    @InjectMocks
    private RemoteFriendFindService service;
    
    @BeforeMethod
    public void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void getFriend() throws ServiceException {
        
        // prepare
        String urlBase = "testBase";
        when(externalPropertiesService.getFindFriendApiBaseUrl()).thenReturn(urlBase);
        Friend expected = new Friend();
        when(restTemplate.getForObject(urlBase + "/v1/test", Friend.class)).thenReturn(expected);
        
        // test
        FriendFindService.Friend actual = service.getFriend();
        
        // assert
        Assert.assertEquals(actual, expected);
    }
}
