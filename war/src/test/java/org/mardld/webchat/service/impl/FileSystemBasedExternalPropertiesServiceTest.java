package org.mardld.webchat.service.impl;

import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;

import org.mardld.webchat.service.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;

public class FileSystemBasedExternalPropertiesServiceTest {
    
    @InjectMocks
    private FileSystemBasedExternalPropertiesService service;
    
    @BeforeMethod
    public void beforeMethod() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        
        String propsFile = MessageFormat.format("..{0}external-dependencies{0}web-chat-service.properties",
                File.separator);
        System.setProperty("webchat.propertiesFile", propsFile);
        MockitoAnnotations.initMocks(this);
    }
    
    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Properties load failed. Required JVM arg 'webchat.propertiesFile' is missing.")
    public void ctor_jvm_missing() throws ServiceException {
        // prepare
        System.clearProperty("webchat.propertiesFile");

        // test
        new FileSystemBasedExternalPropertiesService();
    }
    
    @Test(expectedExceptions = ServiceException.class)
    public void ctor_file_not_found() throws ServiceException {
        // prepare
        System.setProperty("webchat.propertiesFile", ".properties");
        
        // test
        new FileSystemBasedExternalPropertiesService();
    }
    
    @Test
    public void getFindFriendApiBaseUrl() {
        
        // prepare
        String expected = "http://vioflp-acer-v13-pc:8180/friend-find-service/api";
        
        // test
        String actual = service.getFindFriendApiBaseUrl();
        
        // assert
        Assert.assertEquals(actual, expected);
    }
}
