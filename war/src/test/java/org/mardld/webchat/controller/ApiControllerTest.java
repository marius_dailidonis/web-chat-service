package org.mardld.webchat.controller;

import org.testng.annotations.Test;
import org.mardld.webchat.service.FriendFindService;
import org.mardld.webchat.service.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;

public class ApiControllerTest {
    
    @Mock
    private FriendFindService friendFindService;
    
    @InjectMocks
    private ApiController controller;
    
    @BeforeMethod
    public void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void sayHello() {
        
        // test
        SayHelloResponse actual = controller.sayHello();
        
        // assert
        Assert.assertNotNull(actual);
    }
    
    @Test
    public void getFriend() throws ServiceException {
        
        // prepare
        FriendFindService.Friend expected = new FriendFindService.Friend();
        when(friendFindService.getFriend()).thenReturn(expected);
        
        // test
        FriendFindService.Friend actual = controller.getFriend();
        
        // assert
        Assert.assertEquals(actual, expected);
    }
}
