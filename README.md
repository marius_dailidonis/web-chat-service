# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
Execute gradle test command.
* Deployment instructions
Running on server requires a JVM arg 'webchat.propertiesFile' to be provided to read properties.
The example of properties file is provided in external-dependencies folder, named as 'web-chat-service.properties'.

JVM args to set SSL:
-Djavax.net.ssl.trustStore="C:\keystores\acer-webchat-truststore.jks" 
-Djavax.net.ssl.trustStorePassword="changeit"
-Djavax.net.ssl.keyStoreType=pkcs12
-Djavax.net.ssl.keyStore=
-Djavax.net.ssl.keyStorePassword=

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact